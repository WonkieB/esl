from myhdl import *
from spi_module import *


@block
def lcd(clk, reset, spi_input, spi_output, spi_clk, cs_bus, lcd_rs, lcd_reset):

    write_data = Signal(intbv(0, min=0, max=256))
    read_data = Signal(intbv(0, min=0, max=256))
    slave_num = Signal(intbv(0, min=0, max=2))
    command = Signal(bool(1))
    enable = Signal(bool(0))
    ready = Signal(bool(0))

    spi_inst = spi_module(clk, reset, spi_clk, write_data, read_data, spi_input, spi_output, cs_bus, slave_num, command,
                          enable, ready)

    init = Signal(bool(0))

    @always_seq(clk.posedge, reset)
    def lcd_seq():
        if init == 0:
            lcd_rs.next = 0
            lcd_reset.next = 1
            write_data.next = 0x2C
            enable.next = 1
        else:
            write_data.next = 0x0F

    return spi_inst, lcd_seq


p_clk = Signal(bool(0))
p_reset = ResetSignal(1, active=0, isasync=True)
p_spi_input = Signal(bool(0))
p_spi_output = Signal(bool(0))
p_spi_clk = Signal(bool(0))
p_cs_bus = Signal(bool(0))
p_lcd_rs = Signal(bool(0))
p_lcd_reset = Signal(bool(0))

lcd(p_clk, p_reset, p_spi_input, p_spi_output, p_spi_clk, p_cs_bus, p_lcd_rs, p_lcd_reset).convert(hdl="VHDL",
                                                                                                   initial_values=True)
