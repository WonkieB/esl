from myhdl import *
from clock_divider import clock_divider

t_State = enum("ST_IDLE", "ST_INIT", "ST_SEND_DATA", "ST_READ_DATA", "ST_TERMINATE");
t_Read_State = enum("RD_GOING", "RD_FINISHED")
#t_Command = enum("CM_WRITE", "CM_READ")

CLOCK_FREQ = 10000000
SPI_FREQ = 1000000
DIV_FACTOR = int(CLOCK_FREQ/SPI_FREQ)
DATA_WIDTH = 8
SLAVES_NUMBER = 1
CPOL = 0    # clock polarization on idle state - 0 for low logic level
CPHA = 0    # 0 for data set on falling edge and read on rising edge, 1 for opposite

@block
def spi_module(clk_i, reset_i, spi_clk_o, data_i, data_o, spi_i, spi_o, cs_bus_o, slave_num_i, command_i, enable_i, ready_o):

    div_clk = Signal(bool(0))
    int_clk = Signal(bool(0))   # internal clock
    clock_enable = Signal(bool(0))
    read_enable = Signal(bool(0))
    clk_div_inst = clock_divider(clk_i, reset_i, DIV_FACTOR, div_clk)

    state = Signal(t_State.ST_IDLE)
    read_state = Signal(t_Read_State.RD_FINISHED)
    data_counter = Signal(intbv(DATA_WIDTH-1, min=0, max=DATA_WIDTH))
    read_counter = Signal(intbv(DATA_WIDTH - 1, min=0, max=DATA_WIDTH))
    data_i_reg = Signal(intbv(0, min=0, max=2**DATA_WIDTH))
    slave_num_i_reg = Signal(intbv(0, min=0, max=SLAVES_NUMBER + 1))
    command_i_reg = Signal(bool(1))

    @always_comb
    def spi_comb():
        if CPHA == 1:
            int_clk.next = not div_clk
        else:
            int_clk.next = div_clk

        # read new data only when idle
        if ready_o == 1:
            data_i_reg.next = data_i
            slave_num_i_reg.next = slave_num_i
            command_i_reg.next = command_i

        if clock_enable == 1:
            spi_clk_o.next = div_clk
        else:
            spi_clk_o.next = CPOL

    @always_seq(int_clk.negedge, reset_i)
    def spi_seq_neg():

        if state == t_State.ST_IDLE:
            if enable_i:
                state.next = t_State.ST_INIT
                ready_o.next = False
            else:
                ready_o.next = True

        elif state == t_State.ST_INIT:
            cs_bus_o.next = (2**SLAVES_NUMBER-1) & ~(1 << slave_num_i_reg)
            data_counter.next = DATA_WIDTH-1
            if command_i == 1:
                state.next = t_State.ST_SEND_DATA
            elif command_i == 0:
                state.next = t_State.ST_READ_DATA
                read_enable.next = True

            else:
                raise ValueError("Undefined Command")

        elif state == t_State.ST_SEND_DATA:
            spi_o.next = data_i_reg[data_counter]
            clock_enable.next = True
            if data_counter == 0:
                state.next = t_State.ST_TERMINATE
            else:
                data_counter.next = data_counter - 1

        elif state == t_State.ST_READ_DATA:
            if read_state == t_Read_State.RD_GOING:
                clock_enable.next = True
                read_enable.next = False
            elif read_state == t_Read_State.RD_FINISHED:
                state.next = t_State.ST_TERMINATE

        elif state == t_State.ST_TERMINATE:
            clock_enable.next = False
            cs_bus_o.next = (2 ** SLAVES_NUMBER - 1)
            state.next = t_State.ST_IDLE
            ready_o.next = True

        else:
            raise ValueError("Undefined State")

    @always_seq(int_clk.posedge, reset_i)
    def spi_seq_pos():
        if read_enable == True:
            read_state.next = t_Read_State.RD_GOING
            read_counter.next = DATA_WIDTH - 1
            data_o.next = 0

        elif read_state == t_Read_State.RD_GOING:
            data_o.next = data_o + (spi_i << read_counter)
            if read_counter == 0:
                read_state.next = t_Read_State.RD_FINISHED
            else:
                read_counter.next = read_counter - 1

    return clk_div_inst, spi_comb, spi_seq_neg, spi_seq_pos

