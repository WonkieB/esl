from myhdl import *
from math import ceil


@block
def clock_divider(clk_i, reset_i, factor_i, divclk_o):
    """
    This module implements a clock divider.

    Ports:
    -----
    clk_i: clock input
    factor_i: divide factor
    reset_i: global reset
    divclk_o: clock divided by divide factor
    -----

    """

    div_counter = Signal(modbv(0, min=0, max=1024))
    factor_reg = Signal(intbv(factor_i, min=0, max=1024))
    divclk_o_reg = Signal(bool(0))

    @always_comb
    def enable():
        if factor_reg < 2:
            divclk_o.next = clk_i
        else:
            divclk_o.next = divclk_o_reg

    @always_seq(clk_i.negedge, reset=reset_i)
    def divide():
        if div_counter == 0:
            factor_reg.next = factor_i
            divclk_o_reg.next = bool(0)

        if div_counter >= int(factor_reg // 2):
            divclk_o_reg.next = bool(1)

        if div_counter == factor_reg - 1:
            div_counter.next = 0
        else:
            div_counter.next = div_counter + 1

    return enable, divide
