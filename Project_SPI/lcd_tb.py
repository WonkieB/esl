from myhdl import *

from spi_module import *


@block
def spi_tb():

    data_to_read = intbv(0x8F, min=0, max=256)

    clk = Signal(bool(0))
    reset = ResetSignal(0, active=0, isasync=True)
    spi_clk = Signal(bool(0))

    write_data = Signal(intbv(0, min=0, max=256))
    read_data = Signal(intbv(0, min=0, max=256))
    spi_input = Signal(intbv(0, min=0, max=2))
    spi_output = Signal(intbv(0, min=0, max=2))
    cs_bus = Signal(intbv(0, min=0, max=2))
    slave_num = Signal(intbv(0, min=0, max=2))
    command = Signal(bool(1))
    enable = Signal(bool(0))
    ready = Signal(bool(1))

    spi_inst = spi_module(clk, reset, spi_clk, write_data, read_data, spi_input, spi_output, cs_bus, slave_num, command, enable,
                          ready)

    @always(delay(50))
    def clk_gen():
        clk.next = not clk

    @instance
    def stimulus():
        reset.next = 0
        yield clk.negedge
        reset.next = 1
        write_data.next = 0xAA
        enable.next = True
        yield spi_clk.negedge
        enable.next = False
        write_data.next = 0x54
        yield ready.posedge
        enable.next = True
        yield spi_clk.negedge
        enable.next = False
        yield ready.posedge

        # initialize read
        print("Initializing read:", read_data)
        spi_input.next = data_to_read[7]
        command.next = 0
        enable.next = True
        yield spi_clk.negedge
        enable.next = False
        spi_input.next = data_to_read[6]

        for n in range(5, 0, -1):
            yield spi_clk.negedge
            spi_input.next = data_to_read[n]

        yield ready.posedge
        print(read_data)
        assert (data_to_read == read_data)
        raise StopSimulation()

    return spi_inst, clk_gen, stimulus


tb = spi_tb()
tb = traceSignals(spi_tb())
tb.run_sim()

